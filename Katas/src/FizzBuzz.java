public class FizzBuzz {

    public static void main(String[] args) {
        for (int number = 1; number <= 100; number++) {
            if (number%10 ==1) {
                System.out.println();
            }
            if (number%3 == 0 && number %5 ==0) {
                System.out.print("FizzBuzz, ");
            }
            else if (number%3 ==0) {
                System.out.print("Fizz, ");
            }
            else if(number%5 ==0){
                System.out.print("Buzz, ");
            } else {
                System.out.print(number + ", ");
            }
        }
    }
}
