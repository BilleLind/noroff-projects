import java.lang.reflect.Array;
import java.util.HashMap;

public class BookStoreDiscount {

    public static void main(String[] args) {
        int[] books = {0,0,1,1};
        double booksPrice = price(books, 8);
        System.out.println("Price of the books: " + booksPrice);
    }


    public static double price(int[] books, double bookPrice) {
        double totalPrice = 0;
        if (books.length == 0) {
            System.out.println("No Books entered!");
            return totalPrice;
        }

        HashMap<Integer, Integer> uniqueBooks = new HashMap<>();
        for (int book : books) {
            //it is safe to assume that if it is empty then the first book is unique.
            if (uniqueBooks.isEmpty()) {
                System.out.println("First unique");
                uniqueBooks.put(0, book);
                continue;}
            if (!uniqueBooks.containsValue(book)) {
                uniqueBooks.put(uniqueBooks.size(), book);
            } else totalPrice += bookPrice;
        }
        totalPrice += calculateDiscount(uniqueBooks.size(), bookPrice);
        return totalPrice;
    }

    public static double calculateDiscount(int uniqueBook, double bookPrice){
        if (uniqueBook <2) return uniqueBook * bookPrice;
        if (uniqueBook ==2) return (2*bookPrice) * 0.95;
        if (uniqueBook == 3) return (3*bookPrice) * 0.90;
        if (uniqueBook ==4 ) return (4*bookPrice) * 0.80;
        else return ((4*bookPrice) * 0.80 + ((uniqueBook -4) *bookPrice) );
    }
}
