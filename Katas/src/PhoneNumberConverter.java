import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberConverter {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter Phone Number");
        String numberToBeConverted = input.next();

        System.out.println("----------------");
        System.out.println(Phone.textToNum(numberToBeConverted));

    }
}


class Phone {

    static String textToNum(String number) {

        // Makes sure the number is all upperCase and the leading and trailing whitespaces is removed
        String convertedNumber = number.toUpperCase().trim();

        //Went from 3 of the same methods one with A, B then C => inspired by Jack's regex solution
        convertedNumber = regexMatcher(convertedNumber, "[A-C]", "2");

        convertedNumber = regexMatcher(convertedNumber, "[D-F]", "3");

        convertedNumber = regexMatcher(convertedNumber, "[G-I]", "4");

        convertedNumber = regexMatcher(convertedNumber, "[J-L]", "5");

        convertedNumber = regexMatcher(convertedNumber, "[M-O]", "6");

        convertedNumber = regexMatcher(convertedNumber, "[P-S]", "7");

        convertedNumber = regexMatcher(convertedNumber, "[T-V]", "8");

        convertedNumber = regexMatcher(convertedNumber, "[W-Z]", "9");


        return convertedNumber;
    }

    //instead of the using the Java Pattern and Matcher classes String.replaceAll accepts regex
    // it would then be able to be used like => number.replaceAll("[A-C]", "2") making it shorter
    static String regexMatcher (String textToBeConverted, String regex, String replacement) {
        Pattern ABC = Pattern.compile(regex,Pattern.CASE_INSENSITIVE );
        Matcher matcher = ABC.matcher(textToBeConverted);

        return matcher.replaceAll(replacement);
    }
}
