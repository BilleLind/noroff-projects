import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CanComplete {
    public static void main(String[] args) {
        System.out.println("Query result: " + canComplete("btful", "beautiful"));

    }

    public static boolean canComplete(String input, String word) {

        //counter to make substring of word:
        int indexCounter = 0;
        String[] inputArray = input.split("");
        for (int i = 0; i < input.length(); i++) {
            System.out.println("String to be Matched: " + inputArray[i]);

            String wordFromIndex = "";

            if (indexCounter <= word.length()-1) {
                wordFromIndex = word.substring(indexCounter);
            }
            System.out.println("Matched word: "+wordFromIndex);

            if(wordFromIndex.contains(inputArray[i])) {
                indexCounter = word.indexOf(inputArray[i])+1;
            } else {
                return false;
            }
        }
        return true;
    }

}
