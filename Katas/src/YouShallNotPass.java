import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class YouShallNotPass {
    public static void main(String[] args) {
        String password = "15Mb$¤f3cKcg3oh7";


        System.out.println(passwordStrengthChecker(password));
    }

    public static String passwordStrengthChecker( String password){
        int strength = 0;
        if (checkLength(password)) strength++;
        if (checkDigit(password)) strength++;
        if (checkUppercase(password)) strength++;
        if (checkLowercase(password)) strength++;
        if (checkSpecialCharacter(password))strength++;

        if (strength <= 2) return "Weak";
        if (strength <=4) return "Moderate";

        return "Strong";
    }

    public static boolean checkLowercase(String input){
        return Pattern.compile("[a-z+]").matcher(input).find();
    }

    public static boolean checkUppercase(String input){
        return Pattern.compile("[A-Z+]").matcher(input).find();
    }

    public static boolean checkSpecialCharacter(String input){
        //little uncertain on this regex,
        /* Breakdown
            "(?=" is a positive lookahead, asserts the given subpattern can be matched here, without consuming characters
            ".*?"   translates to=> any single character with a lazy quantifier (matches as few characters as possible)
        */

        // "[(?=.+?[#?!@$%^&*-]]" also worked => * to a +
        return Pattern.compile("[(?=.*?[#?!@$%^&*-]]").matcher(input).find();
    }

    //It is possible to use something like \d+ but 0-9 is more simple and readable.
    public static boolean checkDigit(String input){
       return Pattern.compile("[0-9+]").matcher(input).find();
    }

    //KISS => keeping it simple
    public static boolean checkLength(String string) {
        if (string.length()>=8 && !Pattern.compile("\s").matcher(string).find()) return true;
        return false;
    }



}
