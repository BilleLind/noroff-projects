import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookStoreDiscountTest {

    @Test
    void price_validZero_shouldReturnZero() {
        int bookPrice = 8;
        int[] books = {};
        double expected = 0.00;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }
    @Test
    void price_validNrOne_shouldReturn8() {
        int bookPrice = 8;
        int[] books = {1};
        double expected = 8.00;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }

    @Test
    void price_validNrTwo_shouldReturn8() {
        int bookPrice = 8;
        int[] books = {2};
        double expected = 8.00;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }

    @Test
    void price_validNrThree_shouldReturn8() {
        int bookPrice = 8;
        int[] books = {3};
        double expected = 8.00;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }


    @Test
    void price_validThreeOfTheSameBooks_shouldReturn24() {
        int bookPrice = 8;
        int[] books = {1, 1, 1};
        double expected = 24;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }


    @Test
    void price_validTwoUnique_shouldApplyDiscount() {
        int bookPrice = 8;
        int[] books = {0,1};
        double expected = 15.2;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }
    @Test
    void price_validThreeUnique_shouldApplyDiscount() {
        int bookPrice = 8;
        int[] books = {0,1,2};
        double expected = 21.6;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }

    @Test
    void price_validFourUnique_shouldApplyDiscount() {
        int bookPrice = 8;
        int[] books = {0,1,2,3};
        double expected = 25.6;

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }

    @Test
    void price_optionalDiscountOne_shouldApplyDiscount() {
        int bookPrice = 8;
        int[] books = {0,0,1};
        double expected = 8 + (8*2 *0.95);

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }

    @Test
    void price_optionalDiscountTwo_shouldApplyDiscount() {
        int bookPrice = 8;
        int[] books = {0,0,1,1};
        double expected = 8 * 2 + (8 * 2 * 0.95);

        double actual = BookStoreDiscount.price(books, bookPrice);

        assertEquals(expected, actual);
    }
}