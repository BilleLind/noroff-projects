import java.util.Scanner;

public class DrawRectangle {
    public static void main(String[] args) {

        /*
        rectangle("#", 0, 0);
        System.out.println("\n----");
        rectangle("#", 1, 1);
        System.out.println("\n----");
        rectangle("#", 2, 2);
        System.out.println("\n----");
        rectangle("#", 4, 6);
        System.out.println("\n----");
        rectangle("#", 4, 5);
        rectangle("#", 9, 8);
        */

        Scanner scanner = new Scanner(System.in);
        System.out.println("enter Character");
        String character = scanner.next();

        System.out.println("Enter width");
        int width  = scanner.nextInt();

        System.out.println("Enter heigth");
        int height = scanner.nextInt();


        rectangle(character, width, height);

    }

    public static void rectangle(String character, int width, int height) {
        if (width < 0 || height <0) return;

        if ( width > 1 || height > 1) {
            // Starting with width, from left to right, top to bottom
            printRectangle(character, width, height);
        }
        else {
            System.out.println(character);
        }
    }

    public static void printRectangle(String character, int width, int height){
        for (int column= 0; column < width; column++) {
            System.out.println("");
            for (int row = 0; row < height; row++) {
                if(column == 0 || column == width-1) {
                    // prints the leading and should also last line (top bottom)
                    System.out.print(character);
                } else {
                    if(row == 0 || row == height-1){
                        // prints the height, only first and last line(left right),
                        System.out.print(character );
                    } else {
                        //here the hollow is printed, and the optional extra rectangle
                        System.out.print(" ");
                    }
                }
            }
        }
    }
}
