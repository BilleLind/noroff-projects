import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Arrays {

    public static void main(String[] args) {
        // a) 1D of integers initialized with a literal
        int[] firstDimension = {1,2,3,4,5,6};

        // b) 2D of doubles initialized in the standard way
        int[][] twoDimensional = new int[2][4];

        // c) Use indexes to assign values to each array
            firstDimension[0] = 2;

            twoDimensional[0][1] = 10;
            System.out.println("C) => first: " + firstDimension[0] + " second: " + twoDimensional[0][1] );

        // 3. Create and initialize the following collections:
        // a) ArrayList<String>
             ArrayList<String> codingHeroes= new ArrayList<>();

             codingHeroes.add("Kent C. Dods");
             codingHeroes.add("Matteo Collina");
             codingHeroes.add("Anthony Fuu");
             codingHeroes.add("Rich Harris");

        System.out.println("3.a) Current Favorite: " + codingHeroes.get(1));

        // b) HashSet<String>
            HashSet <String> words = new HashSet<>();
            words.add("firs word");
            words.add("second word");
            words.add("third word");
            words.add("... Where were i ?");
            words.add("check");

            boolean containsWord = words.contains("third word");
            int size = words.size();
            System.out.println("3. B) " + "Word check: " + containsWord + " | size: " + size);

        // c) HashMap<Integer, String>
            HashMap<Integer, String> numberAndWord = new HashMap<>();
            numberAndWord.put(1, "one");
            numberAndWord.put(2, "two");
            numberAndWord.put(3, "three");

            String two = numberAndWord.get(2);
            numberAndWord.remove(2);
            boolean twoRemoved = numberAndWord.containsKey(2);
            numberAndWord.put(2, "two");
            //numberAndWord.clear(); //clears the hashmap commented out since i need to loop through it
        System.out.println("3.C) string in second: " + two + "| second removed: " + twoRemoved);

        // d) Explore the various operations for each collection
            // i have done so in the previous tasks


        // Program Flow Tasks

        System.out.println("\n Program Flow Tasks \n");

        System.out.println("one dimensional array");
        for (int number :firstDimension ) {
            System.out.print(number + ", ");
            if (number == 2) break; //Program Flow task 3
        }
        // Using sout for creating a line break
        System.out.println();

        System.out.println("Two dimensional Array");
        for (int row = 0; row < twoDimensional.length; row++){
            System.out.println("\n Row: " + row);
            for (int col = 0; col < twoDimensional[row].length; col++){
                System.out.print(twoDimensional[row][col] +", ");
                if (twoDimensional[row][col] == twoDimensional[1][2]) {
                    System.out.println(twoDimensional[row][col] + " is being changed to 15!");
                    twoDimensional[row][col] = 15;
                }
            }
        }

        System.out.println("\n codingHeroes: ArrayList");

        for (String hero: codingHeroes){
            System.out.print(hero + ", ");
        }

        System.out.println("\n HashSet Loop");

        //stream() is used to process collections of objects, introduced in java 8.
            // I could remove the Stream(), so it is just forEach() now : From stream().forEach()
        // Take inputs from Collections, Arrays or I/o Channel
        words.forEach(word -> System.out.print(word + ", "));


        System.out.println("\nHashmap: first: values, then keys and at last entrySet for both");

        System.out.println("Values...");
        for (String value: numberAndWord.values()) {
            System.out.print(value + ", ");
            if (value.equals("one")) {
                numberAndWord.put(4, "four");
                System.out.print("Fourth is now created!,\n breaking to avoid crash due to the fact that i changed numberAndWord while in a loop");
                break;
            }
        }

        System.out.println("Keys....");
        for (int key: numberAndWord.keySet()) {
            System.out.print(key + ", ");
            if (key == 2) {
                System.out.print(" key 2! ");
            }
        }

        System.out.println("Both Keys and Values... but with key +1");
        for (Map.Entry<Integer, String> entry: numberAndWord.entrySet()) {
            int key = entry.getKey();
            key += 1;
            String value = entry.getValue() ;
            System.out.print(" Key: " + key + " Value: " + value);
        }

    }
}
