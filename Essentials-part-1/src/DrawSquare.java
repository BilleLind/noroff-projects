import java.util.Scanner;

public class DrawSquare {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter the width of the square");

        int squareWidth = input.nextInt();

        System.out.println("Printing a square with " + squareWidth + " * " + squareWidth);
        // To prevent insane logical if's i split the single * from the multiple
        if(squareWidth > 1) {

            /*
            * I went with a column based approach, also enabling me to start the next line
            * the most logic is in the row for loop, with first the if prints the first top and bottom *
            * where the next nested if makes it possible to leave the middle of the square empty   */
            for (int column= 0; column < squareWidth; column++) {
                System.out.println("");
                for (int row = 0; row < squareWidth; row++) {
                    if(column == 0 || column == squareWidth-1) {
                        System.out.print(" * ");
                    } else {
                        if(row == 0 || row == squareWidth-1){
                            System.out.print(" * ");
                        } else {
                            System.out.print("   ");
                        }
                    }
                }
            }
        } else {
            System.out.println("*");
        }

    }
}
