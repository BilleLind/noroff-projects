public class Casting {

    public static void main(String[] args) {

        //Widening (Automatic) => since int is 4 bytes and double is 8 bytes
        int myInteger =  11;
        double myDouble = myInteger;


        System.out.println("widening=> My integer: " + myInteger + " | My Double: " + myDouble );

        //Narrowing (manual) => since double is 8 bytes and int is 4 bytes
        //the d indicates it is a double
        double myNarrowDouble = 11.06d;
        int myNarrowInt = (int) myNarrowDouble;

        System.out.println("Narrowing=> My integer: " + myNarrowInt + " | My Double: " + myNarrowDouble );
    }
}
